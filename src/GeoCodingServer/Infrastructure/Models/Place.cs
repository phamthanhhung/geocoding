﻿using System;
using System.Collections.Generic;
using NetTopologySuite.Geometries;

namespace SI.GIS.BuildingServer.Infrastructure.Models
{
    public partial class Place
    {
        public long OsmId { get; set; }
        public char OsmType { get; set; }
        public string Class { get; set; }
        public string Type { get; set; }
        public Dictionary<string, string> Name { get; set; }
        public short? AdminLevel { get; set; }
        public Dictionary<string, string> Address { get; set; }
        public Dictionary<string, string> Extratags { get; set; }
        public Geometry Geometry { get; set; }
    }
}

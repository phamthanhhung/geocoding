﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SI.GIS.BuildingServer.Controllers
{   
    [Route("reverse")]
    public class ReverseGeoCodeController: ControllerBase
    {
        private readonly IConfiguration _configuration;

        public ReverseGeoCodeController(IConfiguration configuration)
        {
            _configuration = configuration ?? throw new ArgumentNullException(nameof(configuration)); ;
        }

        [HttpGet]
        public IActionResult Search([FromQuery] string lat, string lon, string zoom)
        {
            string baseUrl = _configuration.GetSection("NominatimUrl").Value;

            var queryString = HttpContext.Request.QueryString;

            var url = baseUrl + "reverse" + queryString + "&format=json&addressdetails=1&namedetail=1";

            HttpClient http = new HttpClient();

            var data = http.GetAsync(url).Result.Content.ReadAsStringAsync().Result;

            var result = JsonConvert.DeserializeObject<object>(data);

            return Ok(result);
        }
    }
}

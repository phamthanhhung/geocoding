﻿using GIS.Persistence.PostgreSQL.Repositories;
using Microsoft.EntityFrameworkCore;
using SI.GIS.BuildingServer.Core.Repositories.Interface;
using SI.GIS.BuildingServer.Infrastructure.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SI.GIS.BuildingServer.Core.Repositories.Implement
{
    public class MediaRepository : EfRepository<Media>, IMediaRepository
    {
        public MediaRepository(DatabaseContext dbContext) : base(dbContext)
        {
        }
    }
}
